import java.io.*; 
public class  ejercicio7{
    

    public static BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        int fila = 0, numeroMayor = 0;
        int[][] matriz = {{50, 8, 9}, {8, 15, 10}, {5, 10, 11}};
        fila = Menu();
        fila -= 1;
        for (int i = 0; i < matriz.length; i++) {
            if (numeroMayor < matriz[i][fila]) {
                numeroMayor = matriz[i][fila];
            }
        }
        System.out.println("El numero mayor de esa fila es: " + numeroMayor);

    }

    public static int Menu() throws IOException {
        System.out.println("Seleccione una fila");
        System.out.println("1. Primer Fila");
        System.out.println("2. Segunda Fila");
        System.out.println("3. Tercer Fila");

        return Integer.parseInt(bf.readLine());
    }

}