import java.io.*;


public class ejercicio8{

    public static BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

    public static int f1 = 0, f2 = 0;
    public static int[][] matriz = {{50, 8, 9, 5}, {5, 8, 15, 10}, {14, 5, 10, 11}, {55, 12, 14, 16}};

    public static void main(String[] args) throws IOException {

        imprimirMatriz();

        menu();

        intercambiarFilas();

        imprimirMatriz();

    }

    public static void menu() throws IOException {
        System.out.println("Seleccione las filas a intercambiar");
        System.out.println("1. Primer Fila");
        System.out.println("2. Segunda Fila");
        System.out.println("3. Tercer Fila");
        System.out.println("4. Tercer Fila");

        System.out.print("Fila 1: ");
        f1 = Integer.parseInt(bf.readLine());
        System.out.println("");
        System.out.print("Fila 2: ");
        f2 = Integer.parseInt(bf.readLine());

    }

    public static void imprimirMatriz() {
        System.out.println("Matriz");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(" " + matriz[i][j] + " ");
            }
            System.out.println(" ");
            System.out.println(" ");
        }
    }

    public static void intercambiarFilas() {
        f1 -= 1;
        f2 -= 1;
        if ((f1 < matriz.length) && (f2 < matriz[0].length)) {
            int aux;
            for (int i = 0; i < matriz[0].length; i++) {
                aux = matriz[f1][i];
                matriz[f1][i] = matriz[f2][i];
                matriz[f2][i] = aux;
            }
        }
    }
   
}